const express = require('express')
const exphbs = require('express-handlebars');
const homeRoutes = require('./routes/home')
const UsersRoutes = require('./routes/users')

const app = express()

const hbs = exphbs.create({
    defaultLayout: 'main',
    extname: 'hbs'
});
app.engine('hbs', hbs.engine) //регистрируем движок
app.set('view engine','hbs') // используем движок
app.set('views','view') //с какой папкой работать

app.use(express.static('public'));
app.use('/', homeRoutes)
app.use('/users', UsersRoutes)

const PORT = process.env.PORT || 3000

app.listen(PORT, () =>{
    console.log('Serever is running on port ' + PORT);
})